#!/bin/bash

# Database credentials
user=
password=
host=
db_name=
prefix=

# Other options
backup_path="/var/backups/database/$db_name/"
filename=`date +"%Y%m%d%H%M"`_$db_name

# This parametr set if you wanna delete old files (TRUE or FALSE)
delete_old_files=
# Specify how old files you wanna delete, if parameter delete_old_files is TRUE
days=60

# Check if $backup_path exist
if [ ! -d /var/backups/ ]
then
        mkdir /var/backups
        echo "[ CREATED ] Directory /var/backups created!"
else
        echo "[ OK ] Directory /var/backups/ exist!"
fi

if [ ! -d /var/backups/database/ ]
then
        mkdir /var/backups/database
        echo "[ CREATED ] Directory /var/backups/database created!"
else
        echo "[ OK ] Directory /var/backups/database/ exist!"
fi

if [ ! -d /var/backups/database/$db_name/ ]
then
        mkdir /var/backups/database/$db_name
        echo "[ CREATED ] Directory /var/backups/database/$db_name created!"

	#Change owner and permission to $user of backup directory for removing backup files
        chown $user /var/backups/database/$db_name

        backup="TRUE"
else
        echo "[ OK ] Directory $backup_path complete!"
        backup="TRUE"
fi

if [ "$backup" == "TRUE" ]
then
        echo -e "\n[ RUNNING.. ] Backup process started.."
        # Dump database into SQL file
        mysqldump --user= --password= --host= $db_name > $backup_path/$filename.sql

        # Change owner and permission to $user
        chown $user $backup_path/$filename.sql
        chmod 700 $backup_path/$filename.sql

        echo -e "\n[ DONE ] Backup file $filename.sql is complete in $backup_path!"

# CRC Begin

#	# Make CRC32 checksum file
#       crc32 $backup_path/$filename.sql > $backup_path/$filename-checksum.txt
#
#       #Change owner and permission to $user for checksum file
#       chown $user $backup_path/$filename-checksum.txt
#       chmod 700 $backup_path/$filename-checksum.txt
#
#       echo -e "[ DONE ] Checksum CRC32 file of $filename.zip is complete in $backup_path!\n"

# CRC End

        if [ "$delete_old_files" == "TRUE" ]
        then
                # Delete files older than $days days
                find $backup_path/* -mtime +$days -exec rm {} \;
                echo "Files older than $days days was removed!"
        fi
else
        echo "[ ERROR ] Something went wrong! Backup is not complete!"
fi
