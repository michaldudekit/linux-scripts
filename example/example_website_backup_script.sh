#!/bin/bash

# Website info
user=
dir_name=
prefix=

# Other options
backup_path="/var/backups/www/$dir_name"
filename=`date +"%Y%m%d%H%M"`$prefix$dir_name

# This parametr set if you wanna delete old files (TRUE or FALSE)
delete_old_files=
# Specify how old files you wanna delete, if parameter delete_old_files is TRUE
days=60

# Check if $backup_path exist
if [ ! -d /var/backups/ ]
then
        mkdir /var/backups
        echo "[ CREATED ] Directory /var/backups created!"
else
        echo "[ OK ] Directory /var/backups/ exist!"
fi

if [ ! -d /var/backups/www/ ]
then
        mkdir /var/backups/www
        echo "[ CREATED ] Directory /var/backups/www created!"
else
        echo "[ OK ] Directory /var/backups/www/ exist!"
fi

if [ ! -d /var/backups/www/$dir_name/ ]
then
        mkdir /var/backups/www/$dir_name
        echo "[ CREATED ] Directory /var/backups/www/$dir_name created!"

	#Change owner and permission to $user for backup directory for removing of backup files
        chown $user /var/backups/www/$dir_name
        backup="TRUE"
else
        echo "[ OK ] Directory $backup_path complete!"
        backup="TRUE"
fi

if [ "$backup" == "TRUE" ]
then

        echo -e "\n[ RUNNING.. ] Backup process started.."
        # Archive website
        cd /var/www/$dir_name
        zip -q -r $backup_path/$filename.zip ./

        #Change owner and permission to $user for ZIP file
        chown $user $backup_path/$filename.zip
        chmod 700 $backup_path/$filename.zip

        echo -e "\n[ DONE ] Backup file $filename.zip is complete in $backup_path!"

	# Make CRC32 checksum file
	crc32 $backup_path/$filename.zip > $backup_path/$filename-checksum.txt

	#Change owner and permission to $user for checksum file
        chown $user $backup_path/$filename-checksum.txt
        chmod 700 $backup_path/$filename-checksum.txt

        echo -e "[ DONE ] Checksum CRC32 file of $filename.zip is complete in $backup_path!\n"

        if [ "$delete_old_files" == "TRUE" ]
        then
                # Delete files older than $days days
                find $backup_path/* -mtime +$days -exec rm {} \;
                echo "Files older than $days days was removed!"
        fi
else
        echo "[ ERROR ] Something went wrong! Backup is not complete!"
fi

