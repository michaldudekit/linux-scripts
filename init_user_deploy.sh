#!/bin/bash

## Script for easy made user deployment
# Author: Michal Dudek - www.michaldudek.it
# Version: 0.2
# https://gitlab.com/michaldudekit/linux-scripts

# Features:
# - Creating user [adduser]
# - Creating .ssh folder with authorized_keys for PublicKey authentification
# - Grant sudo privileges to created user (optional)
# - PublicKey deployment from GoogleDisk with auto-sync in crontab (optional)

## Checking prerequisiies
echo -e "[ PREREQUISITES ] Testing for prerequisites.. "
if ! command -v adduser &> /dev/null
then
        echo "[ ERROR ] Adduser could not be found!"
        read -p "Do you want to install? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit
        sudo apt -y install adduser
else
        echo -e "[ OK ] Adduser is here!"
fi

if ! command -v sudo &> /dev/null
then
        echo "[ ERROR ] Sudo could not be found!"
        read -p "Do you want to install? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit
        sudo apt -y install sudo
else
        echo -e "[ OK ] Sudo is here!"
fi

if ! command -v wget &> /dev/null
then
        echo "[ ERROR ] Wget could not be found!"
        read -p "Do you want to install? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit
        sudo apt -y install wget
else
        echo -e "[ OK ] Wget is here!"
fi

## Adding username
echo -e "\n[ RUNNING ] Running deploy script.."
read -p "Insert username to add in to your computer: " username

if id "$username" >/dev/null 2>&1; then
        echo -e "[ ERROR ] User already exist!\n"
else
	adduser $username
        echo "[ OK ] User $username succesfully created!"
	mkdir /home/$username/.ssh
	echo "" >> /home/$username/.ssh/authorized_keys
	echo -e "[ OK ] User directory .ssh with authorized_keys created!"
	chown $username:$username /home/$username/.ssh -R
	chmod 700 /home/$username/.ssh -R
	chmod 600 /home/$username/.ssh/authorized_keys -R
	echo -e "[ OK ] Permission granted to $username!\n"
fi

## Copy PublicKey manualy
read -p "Do you want to copy SSH PublicKey manualy? (y/n): " pubkey
if [ "$pubkey" == "y" ] || [ "$pubkey" == "Y" ]
then
        read -p "Enter your PublicKey here: " pubkey
	echo $pubkey > /home/$username/.ssh/authorized_keys
	chown $username:$username /home/$username/.ssh -R
	chmod 700 /home/$username/.ssh -R
	chmod 600 /home/$username/.ssh/authorized_keys -R
	echo -e "[ OK ] PublicKey created!\n"
fi

## Grant sudo priviliges to acreated user
read -p "Do you want to grant sudo privileges to $username? (y/n): " sudopriv
if [ "$sudopriv" == "y" ] || [ "$sudopriv" == "Y" ]
then
	usermod -aG sudo $username
	echo -e "[ OK ] Sudo privileges granted to user $username!"
fi

## Setting PublicKey auto-sync from GoogleDrive
echo -e "\n[ INFO ] You can use auto-sync your PublicKey from Google Drive. You will need to create txt file into your Google Drive, set to public access with link and copy your FILEID from that link here.\n"
read -p "Do you want to setup auto-sync PublicKey with Google Drive? (y/n): " gdrivekey
if [ "$gdrivekey" == "y" ] || [ "$gdrivekey" == "Y" ]
then
        read -p "Enter your PublicKey FILEID: " fileid
	mkdir /home/$username/.ssh/auto-sync
	cp ./auto-sync.sh /home/$username/.ssh/auto-sync/
	chown $username /home/$username/.ssh/auto-sync -R
	chmod +x /home/$username/.ssh/auto-sync/auto-sync.sh

	# Copy var to auto-sync.sh
	sed -i "s/username=/username="$username"/" /home/$username/.ssh/auto-sync/auto-sync.sh
	sed -i "s/fileid=/fileid="$fileid"/" /home/$username/.ssh/auto-sync/auto-sync.sh

	# Testing
	echo -e "[ TEST ] Testing auto-sync and FILEID..  "
	/home/$username/.ssh/auto-sync/auto-sync.sh
	echo -e "[ CREATE ] Creating Cronjob for auto-sync.."
	crontab -l > tmp-cron
	echo -e "\n# Auto-sync publickey from GoogleDrive for $username every 10min." >> tmp-cron
	echo "*/10 * * * * /home/$username/.ssh/auto-sync/auto-sync.sh" >> tmp-cron
	crontab tmp-cron
	rm tmp-cron
	echo -e "[ OK ] Cronjob in root crontab for user $username is created!"
fi
echo -e "\n[ COMPLETE ] User deployment is complete!"

