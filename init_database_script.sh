#!/bin/bash
echo -e "[ PREREQUISITES ] Testing for prerequisites.. "
if ! command -v mysqldump &> /dev/null
then
        echo "Program mysqldump could not be found!"
        read -p "Do you want to install? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
	sudo apt-get -y install mysql-client
else
        echo -e "[ OK ] Program mysqldump is here!"
fi


echo -e "\n[ INITIALIZING ] Initializing information of DATABASE backup script..\n"
# Default value for host
host="localhost"
# Default value for prefix
prefix="_DB_"
# Default value for delete_old_files 60 days
days=60

# Enter credentials of new database
read -p "Enter full database name: " db_name
read -p "Enter username of user who has permission to backup database $db_name: " user
read -p "Enter password for $user: " -s password
echo -e "\n"
read -p "Enter location of database (default: localhost): " -i $host

## Optional settings
# Settings of deleting old backup files in backup directory
read -p "Do you want to delete old backup files? (y/n): " delete_old_files

if [ "$delete_old_files" == "y" ] || [ "$delete_old_files" == "Y" ]
then
	delete_old_files="TRUE"
	read -p "How many days you want to keep old backup files in backup folder? (60): " -i $days -e days
else
	delete_old_files="FALSE"
	echo "Backup script WILL NOT delete old backup files. You can still change this option manualy in the script."
fi

# Settings of backup file prefix
echo -e "\nBackup file will look like YYYYMMDDHHMM_$db_name.sql OR YYYMMDDHHMM_prefix_$db_name.sql."
read -p "Do you want to use prefix for your backup file? (y/n): " prefix

if [ "$prefix" == "y" ] || [ "$prefix" == "Y" ]
then
        read -p "Enter your file prefix (include _ if you want): " prefix
else
	prefix="_"
        echo "Backup script WILL NOT use file prefix. You can still change this option manualy in the script."
fi


echo -e "\n[ SUMMARY ] Your configuration is: \n"
echo "Database to backup: $db_name"
echo "Backup username: $user"
echo "Database location: $host"
echo -e "Backup file will look like: YYYYMMDDHHMM$prefix$db_name.sql"

read -p "Continue? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1

echo -e "\n[ RUNNING ] Initializing DATABASE backup script.."
# Make a copy of example file
cp ./example/example_database_backup_script.sh ./backup_db_$db_name.sh
chmod +x ./backup_db_$db_name.sh
backup_file="./backup_db_$db_name.sh"


# Change example credentials to USER_INPUT
sed -i "s/user=/user="$user"/" $backup_file
sed -i "s/password=/password="$password"/" $backup_file
sed -i "s/host=/host="$host"/" $backup_file
sed -i "s/db_name=/db_name="$db_name"/" $backup_file

sed -i "s/prefix=/prefix="$prefix"/" $backup_file
sed -i "s/delete_old_files=/delete_old_files="$delete_old_files"/" $backup_file
sed -i "s/days=60/days=$days/" $backup_file

echo -e "[ DONE ] Backup script backup_db_$db_name.sh is succesfully created!\n"

read -p "Do you want to test backup script? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
./backup_db_$db_name.sh

# Cronjob setup
read -p "Do you want to set cronjob for automatic backup at 1:00am every day? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
echo -e "[ CREATE ] Creating cronjob for automatic DATABASE backup.."
crontab -l > tmp-cron
echo -e "\n# Backup database $db_name at 1:00am every day" >> tmp-cron
echo "0 1 * * * $HOME/scripts/backup_db_$db_name.sh" >> tmp-cron
crontab tmp-cron
rm tmp-cron
echo -e "[ OK ] Cronjob in $USER crontab is created!"
