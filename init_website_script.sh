#!/bin/bash
echo -e "[ PREREQUISITES ] Testing for prerequisites.. "
if ! command -v zip &> /dev/null
then
	echo "Program zip could not be found!"
	read -p "Do you want to install? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
	sudo apt -y install zip
else
	echo -e "[ OK ] Program zip is here!"
fi

if ! command -v unzip &> /dev/null
then
        echo "Program unzip could not be found!"
        read -p "Do you want to install? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
        sudo apt -y install unzip
else
        echo -e "[ OK ] Program unzip is here!"
fi

if ! command -v crc32 &> /dev/null
then
        echo "ZIP library for CRC32 could not be found!"
        read -p "Do you want to install? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
        sudo apt -y install libarchive-zip-perl
else
        echo -e "[ OK ] Library for CRC32 checksum is here!"
fi

echo -e "\n[ INITIALIZING ] Initializing information of WEBSITE backup script..\n"
# Default value for delete_old_files
days=60

# Enter credentials of new database
read -p "Enter website name in directory /var/www for backup: " web_name
read -p "Enter name of linux user who has acces to $web_name: " user

## Set default options
# Settings of deleting old backup files in backup directory
read -p "Do you want to delete old backup files? (y/n): " delete_old_files

if [ "$delete_old_files" == "y" ] || [ "$delete_old_files" == "Y" ]
then
	delete_old_files="TRUE"
	read -p "How many days you want to keep old backup files in backup folder? (60): " -i $days -e days
else
	delete_old_files="FALSE"
	echo "Backup script WILL NOT delete old backup files. You can still change this option manualy in the script."
fi

# Settings of backup file prefix
echo -e "\nBackup file will look like YYYYMMDDHHMM_$web_name.zip OR YYYYMMDDHHMM_prefix_$web_name.zip"
read -p "Do you want to use prefix for your backup file? (y/n): " prefix

if [ "$prefix" == "y" ] || [ "$prefix" == "Y" ]
then
        read -p "Enter your file prefix (include _ if you want): " prefix
else
	prefix="_"
        echo "Backup script WILL NOT use file prefix. You can still change this option manualy in the script."
fi


echo -e "\n[ SUMMARY ] Your configuration is: \n"
echo "Website name: $web_name"
echo "Backup username: $user"
echo -e "Backup file will look like: YYYYYMMDDHHMM$prefix$web_name.zip"

read -p "Continue? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1

echo -e "\n[ RUNNING ] Initializing WEBSITE backup script.."
# Make a copy of example file
cp ./example/example_website_backup_script.sh ./backup_web_$web_name.sh
chmod +x ./backup_web_$web_name.sh
backup_file="./backup_web_$web_name.sh"


# Change example credentials to USER_INPUT
sed -i "s/user=/user="$user"/" $backup_file
sed -i "s/host=/host="$host"/" $backup_file
sed -i "s/dir_name=/dir_name="$web_name"/" $backup_file

sed -i "s/prefix=/prefix="$prefix"/" $backup_file
sed -i "s/delete_old_files=/delete_old_files="$delete_old_files"/" $backup_file
sed -i "s/days=60/days=$days/" $backup_file

echo -e "[ DONE ] Backup script backup_website_$web_name.sh is succesfully created!\n"

read -p "Do you wan to test backup script? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
./backup_web_$web_name.sh

# Cronjob setup
read -p "Do you want to set cronjob for automatic backup on 1st day of month at 1:30em? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
echo -e "[ CREATE ] Creating cronjob for automatic WEBSITE backup.."
crontab -l > tmp-cron
echo -e "\n# Backup website $web_name on 1st day of month at 1:30am" >> tmp-cron
echo "30 1 1 * * $HOME/scripts/backup_web_$web_name.sh" >> tmp-cron
crontab tmp-cron
rm tmp-cron
echo -e "[ OK ] Cronjob in $USER crontab is created!"
