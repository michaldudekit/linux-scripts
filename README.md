# Linux scripts
Are scripts made for my personal purpose, to set up a backup database or website automaticaly using cron. Also for deploy new user into Linux enviroment, set PublicKey and automatic synchronization of PublicKey from GoogleDrive.

This repository contains:
- User deployment
    - `init_user_deploy.sh`
- Auto-sync script for PublicKey from GoogleDrive
    - `auto-sync.sh`
- Backup scripts for website and database automatic backup
    - `init_database_script.sh`
    - `init_website_script.sh`
    - `example/example_website_backup_script.sh`
    - `example/example_database_backup_script.sh`

## User deployment
Initial script check for depencies `adduser`, `sudo`, `wget`. Using `adduser` to add new username into Linux enviroment. Adduser create `$HOME` directory, `/etc/passwd` and `/etc/shadow` record. Next create `$HOME/.ssh` directory with `authorized_keys` for SSH authentification. Following options are optional. You can directly insert PublicKey for new user, grant sudo permisson and also use script for set up automatic synchronization your PublicKey from GoogleDrive using cron. For this I use Auto-sync script.

## Auto-sync script for PublicKey from GoogleDrive
Script synchronize **(overwrite!)** your PublicKey from GoogleDrive into `$HOME/.ssh/authorized_keys`. For using script you will need:
1. Create txt file with your PublicKey e.g.: `my-key.txt`
2. Upload file `my-key.txt` into your GoogleDrive
3. Change permission of `my-key.txt` to public access with link.
4. Get `FILEID` from public link like: `1iveclIb-T0L_2ETTEs5u4K4OvFuNzVEbXkLuC5UXjCQ`
5. Run script and insert this `FILEID`

**!IMPORTANT** This script dont use cronjob for auto-sync. This feature is available using `init_user_deploy.sh` **OR** add this following into crontab manualy. *Watch out for *`/home/$username/.ssh/auto-sync/`* directory and *`auto-sync.sh`*, just **replace right** *`/path/to/auto-sync-script`* and *`$username`.
```
crontab -e
```
```
# Auto-sync publickey from GoogleDrive for $username every 10min.
*/10 * * * * /home/$username/.ssh/auto-sync/auto-sync.sh
```

## Backup scripts
Initial scripts checks for depencies like `zip`, `unzip` and `mysqldump` for mysql-like DBMS. After checked depencies the initial script start getting `USER_INPUT`. If `USER_INPUT`s are valid and agreed, script will continue to create website or database backup script from `./example`. This script will be stored directly into same folder.
Backup scripts create backup directory into `/var/backup/database/$name_of_database` and for website `/var/backup/www/$name_of_website`.

### Using scripts
**!IMPORTANT** Before running scripts, please initialize crontab for user who will has scripts into own `$HOME/scripts`. For initialize crontab use:
```
crontab -e
```
For using scripts, just copy this repo to `$HOME` directory for user who has permission to access to website directory, or copy to wherever you want.
Repo contains `/example` directory, where are default examples, which are used to make useable scripts. **DONT EDIT!**. So first time run "initial" script, fill necessary credentials and script makes copy of examples to useable script into same directory of initial scripts.

Database intial script needs to know: `database` name, which you want to backup, `username` who has permission to backup selected database, `password` of this user and `location` of this database. Next options are voluntary. See scipts.

Website initial script needs to know only name of website directory in `/var/www` directory and `username` of user who has access to `$ROOT_DIR` of website which you want to backup. This user will have rights to access backup files and CRC checksum files.

#### Automate script
Script also contains optional settings to create crontab record to automate backup.
**!IMPORTANT** Before running scripts, please initialize crontab for user who will have scripts into own `$HOME/scripts/`. For initialize crontab use:
```
crontab -e
```

## TODO
- [x] Make crontab record with user prompt in initial script.
- [x] Added CRC32 checksum file.

## Contribution
If you have any sugesstion for improve or just any question, feel free to leave me a comment.