#!/bin/bash

## Variables
username=
fileid=
filename=""$username"_publickey"

echo -e "[ FILEID ] $fileid"
echo -e "[ FILENAME ] $filename\n"

## RUNNING
echo -e "[ RUNNING ] PublicKey from Google Drive for user $username running.."
cd /home/$username/.ssh
wget "https://docs.google.com/uc?export=download&id=$fileid" -O $filename

if [ -f "$filename" ];
then
        echo -e "[ OK ] File $filename exist, FILEID works!"
        mv /home/$username/.ssh/$filename /home/$username/.ssh/authorized_keys
        chown $username:$username /home/$username/.ssh/ -R
        chmod 700 /home/$username/.ssh
        chmod 600 /home/$username/.ssh/authorized_keys
        echo -e "[ OK ] Authorized_keys successfully updated for user $username!"
else
        echo -e "[ FAILED ] File $filename does not exist, FILEID failed!"
fi
